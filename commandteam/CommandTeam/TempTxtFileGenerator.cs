﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Word;

namespace CommandTeam
{
    public static class TempTxtFileGenerator
    {
        public static void Generate(string docName, string txtName)
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Object fileName = docName;

            Object missing = Type.Missing;
            app.Documents.Open(ref fileName);

            Document doc = app.ActiveDocument;

            object format = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatText;
            object Unknown = Type.Missing;
            int count = doc.Paragraphs.Count;
            doc.GrammarChecked = false;

            var strMas = new string[count];

            for (int i = 0; i < count; i++)
            {
                var par = doc.Paragraphs[i + 1];

                bool flagInTable = true;

                // Лучше так узнавать, принадлежит ли элемент таблице, так как Tables.count дольше выполняется
                try
                {
                    var x = par.Range.Tables[1];
                }
                catch
                {
                    flagInTable = false;
                }

                string text = par.Range.Text.ToString();

                float leftIndent = par.LeftIndent; //слева отступ
                float firstLineIndent = par.FirstLineIndent; //выступ первой строки
                string align;
                if (par.Alignment == WdParagraphAlignment.wdAlignParagraphCenter)  //выравнивание по центру?
                    align = "True";
                else
                    align = "False";
                bool bold = par.Range.Bold == -1;  //жирный шрифт
                bool italic = par.Range.Italic == -1;  //курсив


                strMas[i] = text + "^" + (leftIndent).ToString() + "^" + firstLineIndent.ToString() + "^" + bold + "^" + align + "^" + flagInTable + "^" + italic + "\r\n";
            }



            StreamWriter f = new StreamWriter(txtName);
            for (int i = 0; i < count; i++)
                f.Write(strMas[i]);
            f.Close();
            doc.Close();
            app.Quit(ref Unknown, ref Unknown, ref Unknown);
        }
    }
}
