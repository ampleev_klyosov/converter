﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CommandTeam
{
    public static class Index
    {

        public static void getIndexAndText(string indexWithText, ref string index, ref string text)
        {
            index = "";
            text = "";
            int i = 0;

            string str = indexWithText;
            // Если не "Например:"
            if ((str[0] == 120) || (str[0] == 1093) || (str[0] == '!') || (str[0] == '+') || (str[0] == '-') || (str[0] == '−') || (str[0] == '–'))
                i = 2;

            indexWithText += " ";

            while (indexWithText[i] != ' ')
            {
                index += indexWithText[i];
                i++;
            }

            if (indexWithText[i] == ' ')
                i += 1;

            for (; i < indexWithText.Length; i++)
                text += indexWithText[i];
        }

        public static void LinkGetIndexAndText(string LinkWithText, ref List<string> LinkValueList, ref string text)
        {
            string index = "";
            text = "";
            int i = 2;

            LinkWithText += " ";

            // количество запятых + 1 = количество ссылок
            int kol = 0;
            for (; i < LinkWithText.Length; i++)
                if ((LinkWithText[i] == ',') || (LinkWithText[i] == ';'))
                    ++kol;
            i = 2;

            // одна ссылка
            if (kol == 0)
            {
                int kol2 = 0; // количество пробелов (если один пробел, то ссылка без текста. Если больше одного, то ссылка с текстом)
                for (; i < LinkWithText.Length; i++)
                    if (LinkWithText[i] == ' ')
                        ++kol2;
                i = 2;

                // получаем value ссылки (она одна)                
                while (LinkWithText[i] != ' ')
                {
                    index += LinkWithText[i];
                    i++;
                }
                LinkValueList.Add(index);

                // ссылка с текстом (всегда одно Value, так как присутствует текст)
                if (kol2 > 1)
                {
                    if (LinkWithText[i] == ' ')
                        i += 1;
                    for (; i < LinkWithText.Length; i++)
                        text += LinkWithText[i];
                }
            }

            i = 1; // пропускаем позицию стрелочки

            // если ссылок больше одной
            if (kol > 0)
            {
                LinkWithText = LinkWithText.Replace(", ", " ");
                LinkWithText = LinkWithText.Replace("; ", " ");
                var temp = LinkWithText.Split(' ');
                while (kol + 1 != 0)
                {
                    LinkValueList.Add(temp[i]);
                    --kol;
                    ++i;
                }
            }
        }

        public static XmlElement GenerateIndexXmlElement(char act, XmlDocument myXmlDocument)
        {
            XmlElement indexXmlElement = null;

            if ((act == 120) || (act == 1093))
                indexXmlElement = myXmlDocument.CreateElement("xIndex");

            if (act == '!')
                indexXmlElement = myXmlDocument.CreateElement("NewTextIndex");

            if (act == '+')
            {
                indexXmlElement = myXmlDocument.CreateElement("NewIndex");
            }

            if ((act == '-') || (act == '−') || (act == '–'))
            {
                indexXmlElement = myXmlDocument.CreateElement("Index");
            }

            return indexXmlElement;
        }
    }
}
