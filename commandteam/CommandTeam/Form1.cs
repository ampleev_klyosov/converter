﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace CommandTeam
{
    public partial class Converter : Form
    {
        static void saveFile(XmlDocument x, string fileName)
        {
            MemoryStream w = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(fileName, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 1;
            writer.IndentChar = '\t';
            x.Save(writer);
            writer.Close();
        }

        static void TxtToXmlGenerate(string txtName, string xmlName)
        {
            XmlGenerator xmlGenerator = new XmlGenerator();
            var myXmlDocument = xmlGenerator.GenerateXml(txtName);
            saveFile(myXmlDocument, xmlName);
        }

        public Converter()
        {
            InitializeComponent();
        }

        private void DocToTxt_Click(object sender, EventArgs e)
        {
            string str1 = "", str2 = "";

            if (System.Windows.Forms.DialogResult.OK == openFileDialog1.ShowDialog())
            {
                str1 = openFileDialog1.FileName;
                str2 = str1.Replace(".doc", ".txt");
            }

            TempTxtFileGenerator.Generate(str1, str2);
            MessageBox.Show("txt файл получен!");
        }

        private void TxtToXml_Click(object sender, EventArgs e)
        {
            string str1 = "", str2 = "";

            if (System.Windows.Forms.DialogResult.OK == openFileDialog1.ShowDialog())
            {
                str1 = openFileDialog1.FileName;
                str2 = str1.Replace(".txt", ".xml");
            }

            TxtToXmlGenerate(str1, str2);
            MessageBox.Show("xml файл получен");
        }
    }
}
