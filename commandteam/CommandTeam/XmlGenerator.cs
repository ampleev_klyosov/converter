﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace CommandTeam
{
    public class XmlGenerator
    {
        public List<XmlElement> xmlElementList = new List<XmlElement>();
        public List<int> xmlElementListNumInMasString = new List<int>();

        public List<string> masString = new List<string>();
        public List<double> masLeftIndent = new List<double>();
        public List<double> masFirstLineIndent = new List<double>();
        public List<bool> masBold = new List<bool>();
        public List<bool> masCentralAlignment = new List<bool>();
        public List<bool> masIsElementInTable = new List<bool>();
        public List<bool> masItalic = new List<bool>();

        public XmlDocument GenerateXml(string textFile)
        {
            int i;
            XmlDocument myXmlDocument = new XmlDocument();
            XmlDeclaration xmlDeclaration = myXmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = myXmlDocument.CreateElement("Change");
            myXmlDocument.AppendChild(root);

            string oneBigString = File.ReadAllText(textFile, Encoding.GetEncoding("UTF-8"));

            List<string> mas = oneBigString.Split('\n').ToList();

            // Разбираем на части считанный промежуточный текстовый файл
            for (i = 0; i < mas.Count - 1; i++)
            {
                mas[i] = mas[i].Replace("\r", "");
                var temp = mas[i].Split('^');

                masString.Add(temp[0]);
                masLeftIndent.Add(Convert.ToDouble(temp[1]));
                masFirstLineIndent.Add(Convert.ToDouble(temp[2]));

                if (temp[3] == "True")
                    masBold.Add(true);
                else
                    masBold.Add(false);

                if (temp[4] == "True")
                    masCentralAlignment.Add(true);
                else
                    masCentralAlignment.Add(false);

                if (temp[5] == "True")
                    masIsElementInTable.Add(true);
                else
                    masIsElementInTable.Add(false);

                if (temp[6] == "True")
                    masItalic.Add(true);
                else
                    masItalic.Add(false);
            }

            // Удаляем пробелы в начале и в конце, символы переноса каждой строки
            for (i = 0; i < masString.Count(); i++)
                masString[i] = masString[i].Trim();
            for (i = 0; i < masString.Count(); i++)
                masString[i] = masString[i].Replace("\r", "");
            for (i = 0; i < masString.Count(); i++)
                masString[i] = masString[i].Replace("\t", "");
            for (i = 0; i < masString.Count(); i++)
                masString[i] = masString[i].Replace("", "");     // возле индекса 62 почему-то спецсимвол "нота". (он тут не виден в "", но в реальности есть)
            for (i = 0; i < masString.Count(); i++)
                masString[i] = masString[i].Replace("", "");     // тут ещё один спецсимвол(точка чёрная)
            for (i = 0; i < masString.Count(); i++)
                while (masString[i].Contains("  "))
                    masString[i] = masString[i].Replace("  ", " ");


            int m = 0;
            int n = 0;
            int length = masString.Count();
            while (m < length)
            {
                if (masString[n] == "")
                {
                    masString.RemoveAt(n);
                    masLeftIndent.RemoveAt(n);
                    masFirstLineIndent.RemoveAt(n);
                    masBold.RemoveAt(n);
                    masCentralAlignment.RemoveAt(n);
                    masIsElementInTable.RemoveAt(n);
                    masItalic.RemoveAt(n);
                }
                else
                    n++;
                m++;
            }

            // для текстовых файлов, в кот. стрелочка конвертировалась в скобку
            int kol = 0;
            for (i = 0; i < masString.Count(); i++)
                if ((masString[i][0] == '(') && (masString[i][1] == ' ') && (masString[i].Length > 1))
                {
                    masString[i] = masString[i].Replace("(", "→");
                    ++kol;
                }


            string s = "";
            kol = 0;
            // если отсылка находится в середине строки
            for (i = 0; i < masString.Count(); i++)
                if (masString[i].IndexOf("( ") > 0)
                {
                    s = masString[i].Substring(masString[i].IndexOf("( "));
                    masString[i] = masString[i].Remove(masString[i].IndexOf("( "));
                    s = s.Replace("( ", "→ ");
                    masString.Insert(i + 1, s);
                    ++kol;
                }

            s = "";
            kol = 0;
            // если отсылка находится в середине строки
            for (i = 0; i < masString.Count(); i++)
                if (masString[i].IndexOf("→ ") > 0)
                {
                    s = masString[i].Substring(masString[i].IndexOf("→ "));
                    masString[i] = masString[i].Remove(masString[i].IndexOf("→ "));
                    //s = s.Replace("( ", "→ ");
                    masString.Insert(i + 1, s);
                    ++kol;
                }

            i = -1;
            while (i < masString.Count() - 1)
            {
                i++;
                string str = masString[i];

                if (str.Contains("подразделять как") == true)
                {
                    string index1 = "";
                    string index2 = "";

                    str = str.Replace(" подразделять как ", " ");

                    var temp = str.Split(' ');

                    index1 = temp[0];
                    index2 = temp[1];

                    XmlElement xmlElementSubDivAs = myXmlDocument.CreateElement("SubdivideAs");

                    XmlAttribute xmlAttribute = myXmlDocument.CreateAttribute("Index1");
                    xmlAttribute.Value = index1;
                    xmlElementSubDivAs.Attributes.Append(xmlAttribute);

                    XmlAttribute xmlAttribute2 = myXmlDocument.CreateAttribute("Index2");
                    xmlAttribute2.Value = index2;
                    xmlElementSubDivAs.Attributes.Append(xmlAttribute2);

                    xmlElementList[xmlElementList.Count - 1].AppendChild(xmlElementSubDivAs);

                    continue;
                }

                //120 или 1093 это "x"
                if ((str[0] == 120) || (str[0] == 1093) || (str[0] == '!') || (str[0] == '+') || (str[0] == '-') || (str[0] == '−') || (str[0] == '–'))
                {
                    XmlElement indexXmlElement = null;

                    string indexValue = "";
                    string indexName = "";
                    char act = str[0];
                    Index.getIndexAndText(str, ref indexValue, ref indexName);

                    indexXmlElement = Index.GenerateIndexXmlElement(act, myXmlDocument);

                    XmlAttribute xmlAttribute = myXmlDocument.CreateAttribute("Value");
                    xmlAttribute.Value = indexValue;
                    indexXmlElement.Attributes.Append(xmlAttribute);

                    XmlAttribute xmlAttribute2 = myXmlDocument.CreateAttribute("Name");
                    xmlAttribute2.Value = indexName.Trim();
                    indexXmlElement.Attributes.Append(xmlAttribute2);

                    // Добавили элемент в список
                    xmlElementList.Add(indexXmlElement);
                    xmlElementListNumInMasString.Add(i);

                    root.AppendChild(indexXmlElement);

                    continue;
                }

                if (str.Contains("Например:") == true)
                {
                    string value = "";
                    string name = "";

                    ++i;
                    str = masString[i];
                    while ((str[0] != '→') && (str[0] != 120) && (str[0] != 1093) && (str[0] != '!') && (str[0] != '+') && (str[0] != '-') && (str[0] != '−') && (str[0] != '–'))
                    {
                        Index.getIndexAndText(str, ref value, ref name);

                        var indexXmlElement = myXmlDocument.CreateElement("Example");

                        XmlAttribute xmlAttribute = myXmlDocument.CreateAttribute("Value");
                        xmlAttribute.Value = value;
                        indexXmlElement.Attributes.Append(xmlAttribute);

                        XmlAttribute xmlAttribute2 = myXmlDocument.CreateAttribute("Name");
                        xmlAttribute2.Value = name.Trim();
                        indexXmlElement.Attributes.Append(xmlAttribute2);

                        xmlElementList[xmlElementList.Count - 1].AppendChild(indexXmlElement);

                        value = "";
                        name = "";
                        ++i;
                        str = masString[i];
                    }
                    --i;
                    continue;
                }

                if (str[0] == '→')
                {
                    string linkName = "";
                    List<string> LinkValueList = new List<string>();

                    Index.LinkGetIndexAndText(str, ref LinkValueList, ref linkName);

                    int kol_link = LinkValueList.Count;

                    int k = 0;
                    while (kol_link != 0)
                    {

                        var indexXmlElement = myXmlDocument.CreateElement("Link");

                        XmlAttribute xmlAttribute = myXmlDocument.CreateAttribute("Value");
                        xmlAttribute.Value = LinkValueList[k];
                        indexXmlElement.Attributes.Append(xmlAttribute);

                        XmlAttribute xmlAttribute2 = myXmlDocument.CreateAttribute("Name");
                        xmlAttribute2.Value = linkName;
                        indexXmlElement.Attributes.Append(xmlAttribute2);

                        xmlElementList[xmlElementList.Count - 1].AppendChild(indexXmlElement);

                        --kol_link;
                        ++k;
                        linkName = "";
                    }

                    continue;
                }

                // Заносим в комментарий
                if (xmlElementList.Count() != 0)
                {
                    XmlElement xmlElementComment = myXmlDocument.CreateElement("Comment");
                    XmlAttribute xmlAttributeComment = myXmlDocument.CreateAttribute("Value");
                    xmlAttributeComment.Value = str.Trim();
                    xmlElementComment.Attributes.Append(xmlAttributeComment);
                    xmlElementList[xmlElementList.Count - 1].AppendChild(xmlElementComment);
                }
            }

            return myXmlDocument;
        }
    }
}
