﻿namespace CommandTeam
{
    partial class Converter
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.DocToTxt = new System.Windows.Forms.Button();
            this.TxtToXml = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // DocToTxt
            // 
            this.DocToTxt.Location = new System.Drawing.Point(25, 24);
            this.DocToTxt.Name = "DocToTxt";
            this.DocToTxt.Size = new System.Drawing.Size(75, 23);
            this.DocToTxt.TabIndex = 0;
            this.DocToTxt.Text = "DocToTxt";
            this.DocToTxt.UseVisualStyleBackColor = true;
            this.DocToTxt.Click += new System.EventHandler(this.DocToTxt_Click);
            // 
            // TxtToXml
            // 
            this.TxtToXml.Location = new System.Drawing.Point(126, 24);
            this.TxtToXml.Name = "TxtToXml";
            this.TxtToXml.Size = new System.Drawing.Size(75, 23);
            this.TxtToXml.TabIndex = 1;
            this.TxtToXml.Text = "TxtToXml";
            this.TxtToXml.UseVisualStyleBackColor = true;
            this.TxtToXml.Click += new System.EventHandler(this.TxtToXml_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Converter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 75);
            this.Controls.Add(this.TxtToXml);
            this.Controls.Add(this.DocToTxt);
            this.Name = "Converter";
            this.Text = "Converter";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button DocToTxt;
        private System.Windows.Forms.Button TxtToXml;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

