﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommandTeam;
using System.IO;
using System.Xml;
using System.Text;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string s = "х  =03 (Переводные документы. Переводы)";
            TempTxtFileGenerator.Generate("C:\\Users\\1\\Desktop\\CommandTeam\\CommandTeam\\bin\\Debug\\2.doc", "C:\\Users\\1\\Desktop\\CommandTeam\\CommandTeam\\bin\\Debug\\2.txt");
            StreamReader r = new StreamReader("C:\\Users\\1\\Desktop\\CommandTeam\\CommandTeam\\bin\\Debug\\2.txt");
            string resultString = r.ReadLine();
            Assert.AreEqual(s, resultString);
        }
        [TestMethod]
        public void TestMethod2()
        {
            XmlDocument myXmlDocument = new XmlDocument();

            char act = '!';
            XmlElement indexXmlElement = null;
            indexXmlElement = Index.GenerateIndexXmlElement(act, myXmlDocument);

            string s = indexXmlElement.Name;

            Assert.AreEqual("NewTextIndex", s);
        }
        [TestMethod]
        public void TestMethod3()
        {
            XmlDocument myXmlDocument = new XmlDocument();

            char act = '+';
            XmlElement indexXmlElement = null;
            indexXmlElement = Index.GenerateIndexXmlElement(act, myXmlDocument);

            string s = indexXmlElement.Name;

            Assert.AreEqual("NewIndex", s);
        }
        [TestMethod]
        public void TestMethod4()
        {
            XmlDocument myXmlDocument = new XmlDocument();

            char act = '-';
            XmlElement indexXmlElement = null;
            indexXmlElement = Index.GenerateIndexXmlElement(act, myXmlDocument);

            string s = indexXmlElement.Name;

            Assert.AreEqual("Index", s);
        }
        [TestMethod]
        public void TestMethod5()
        {
            XmlDocument myXmlDocument = new XmlDocument();

            int act = 120;
            XmlElement indexXmlElement = null;
            indexXmlElement = Index.GenerateIndexXmlElement(Convert.ToChar(act), myXmlDocument);

            string s = indexXmlElement.Name;

            Assert.AreEqual("xIndex", s);
        }
    }
}

